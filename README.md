# SDR-based phase measurement system
The software contained in this repository is used to implement a synchronous phase measurement system and can be used
in the characterization of PLL/CDR circuits.

## Theory of operation
An ananlog complex downconverter is used to convert the signal under test to a low intermediate frequency. Placing
the signal under test at a quarter of the sample rate has two advantages:
  * Downconversion to baseband for phase measurement using atan2 is trivial, as the LO simplifies to just four constant phases
  * Suppression of the (upconverted) DC offset can be excellent even when just using a length-four moving average

The signal is therefore downconverted, filtered using a moving average filter and then phase information is computed using
atan2(). A simple window trigger is implemented which allows to trigger on deviations from the initial phase.
All DSP operations are multi-threaded and implemented using VOLK kernels for highest performance. A TCP interface for a PULSCAN laser test system is provided.

## Helper scripts
Transients captured by the software are stored in the `data/` directory. Their filename identifies run number, trigger number and time of acquisition. Two scripts are available for reading / post processing:
  * `read_transient.py` reads a single transient file and produces an interactive plot for inspection
  * `generate_plots.py` monitors the data directory and creates plots for each event
    * this script requires the Python `watchdog` package.
