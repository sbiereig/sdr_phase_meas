#!/usr/bin/env python
#
# The bladeRF v2 uses two fractional-N PLLs with different moduli for generating the
# RF and BB clocks. For coherent operation of the phase measurement DSP chain, generating
# two clocks that satisfy the following constraints are required:
#   * Low-side LO: f_LO = f_RF - f_S / 4 OR
#   * High-side LO: f_LO = f_RF - f_S / 4
#   * BBPLL frequency within range of BBVCO
#   * RFPLL frequency within range of RFVCO
#   * No rounding error for BBPLL and RFPLL frequencies (rational relationship)
from z3 import *

set_option(timeout=900000)

fref_rfpll = 76800000
fref_bbpll = 38400000

mod_rfpll = 8388593
mod_bbpll = 2088960

range_bbvco = {"min": 715000000, "max": 1430000000}
range_rfvco = {"min": 6000000000, "max": 12000000000}

div_rfpll_list = [2, 4, 8, 16, 32, 64, 128]
div_bbpll_list = [2, 4, 8, 16, 32, 64, 128, 256, 512]

n_bbpll = Int('n_bbpll')
d_bbpll = Int('d_bbpll')

n_rfpll = Int('n_rfpll')
d_rfpll = Int('d_rfpll')

f_rfvco = Real('f_rfvco')
f_bbvco = Real('f_bbvco')

f_rfpll = Real('f_rfpll')
f_bbpll = Real('f_bbpll')


f_c_mhz_list = [80, 160, 320, 640, 1280]
f_s_target_mhz_list = range(4, 32)

for f_c_mhz in f_c_mhz_list:
    for f_s_target_mhz in f_s_target_mhz_list:
        f_c = int(f_c_mhz * 1000000)
        f_s_target = int(f_s_target_mhz * 1000000)

        div_rfpll = 0
        div_bbpll = 0
        for div_rfpll_cand in div_rfpll_list:
            if range_rfvco["min"] <= f_c * div_rfpll_cand <= range_rfvco["max"]:
                div_rfpll = div_rfpll_cand

        for div_bbpll_cand in div_bbpll_list:
            if range_bbvco["min"] <= f_s_target * div_bbpll_cand <= range_bbvco["max"]:
                div_bbpll = div_bbpll_cand

        assert div_rfpll != 0
        assert div_bbpll != 0

        s = Solver()
        s.add(
            f_bbpll > (f_s_target - 500000), f_bbpll <= (f_s_target + 500000),

            # divider numerator must be smaller than modulus
            0 <= d_rfpll, d_rfpll < mod_rfpll,
            0 <= d_bbpll, d_bbpll < mod_bbpll,

            # PLL VCO frequency equations
            f_rfpll == fref_rfpll * (n_rfpll + d_rfpll * Q(1, mod_rfpll)) / div_rfpll, 
            f_bbpll == fref_bbpll * (n_bbpll + d_bbpll * Q(1, mod_bbpll)) / div_bbpll,

            # RF-to-BB frequency ratio constraint
            Or(
                # low-side LO
                f_rfpll == f_c - f_bbpll * Q(1, 4),
                # high-side LO
                f_rfpll == f_c + f_bbpll * Q(1, 4)
            ),
            f_rfpll > f_bbpll,
        )
        sat = s.check()
        if str(sat) == "sat":
            m = s.model()
            print(f"Goal: f_c: {f_c_mhz} MHz; f_s: {f_s_target_mhz} MHz. Solution: f_rf: {m[f_rfpll]} Hz; f_s: {m[f_bbpll]} Hz (RF: N={m[n_rfpll]}+{m[d_rfpll]}/{mod_rfpll}*{div_rfpll}; BB: N={m[n_bbpll]}+{m[d_bbpll]}/{mod_bbpll}*{div_bbpll})")

