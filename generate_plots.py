#!/usr/bin/python3
import argparse
import logging
import time
import re
import os

import numpy as np
import matplotlib.pyplot as plt

from watchdog.observers import Observer
from watchdog.events import RegexMatchingEventHandler

class PlotUpdater(object): 
    """Mixin class to reuse checking and plotting logic"""
    @staticmethod
    def check_and_plot(datfile, run_plot_folder):
        pngfile = re.findall(r"trigger\d{6}_\d{8}_\d{6}", datfile)[0] + ".png"
        pngfile = os.path.join(run_plot_folder, pngfile)

        if os.path.exists(pngfile) and os.path.getmtime(pngfile) >= os.path.getmtime(datfile):
            logging.info("%s already up to date, not regenerating.", pngfile)
        else:
            logging.info("Generating %s.", pngfile)
            fs = 40e6
            f_in = 320e6
            
            data = np.fromfile(datfile, dtype=np.float32)
            data = np.unwrap(data)
            data = data / (2 * np.pi * f_in) * 1e9
            t = 1.0 / fs * np.arange(len(data)) * 1e6
            
            plt.figure(figsize=[16, 8])
            plt.plot(t, data)
            plt.xlabel("Time [us]")
            plt.ylabel("Phase [ns]")
            plt.grid()
            plt.savefig(pngfile, dpi=300)
            plt.close()

class TransientDataHandler(RegexMatchingEventHandler, PlotUpdater):
    def __init__(self, run_number, run_plot_folder):
        self._run_plot_folder = run_plot_folder
        super(TransientDataHandler, self).__init__(regexes=[r".*/run%06d_trigger\d{6}_\d{8}_\d{6}.dat$" % run_number],
                ignore_directories=True,
                case_sensitive=True)

    def on_modified(self, event):
        super(RegexMatchingEventHandler, self).on_created(event)
        self.check_and_plot(event.src_path, self._run_plot_folder)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')

    parser = argparse.ArgumentParser(description="Automatically generate plots from data files.")
    parser.add_argument("data_folder", type=str, help="Folder containing data files")
    parser.add_argument("plot_folder", type=str, help="Folder to generate plots to")
    parser.add_argument("run_number", type=int, help="Number of run to plot")
    args = parser.parse_args()

    if not os.path.isdir(args.data_folder):
        raise ValueError("Data folder does not exist or is not a folder.")

    if not os.path.isdir(args.plot_folder):
        os.mkdir(args.plot_folder)
        logging.info("Created plot folder.")

    run_plot_folder = os.path.join(args.plot_folder, "run_%06d" % args.run_number)
    if not os.path.isdir(run_plot_folder):
        os.mkdir(run_plot_folder)
        logging.info("Created plot-subfolder for run %d" % args.run_number)


    # get initial list of files and regenerate all required transients
    files = [f for f in os.listdir(args.data_folder) if re.match(r'run%06d_trigger\d{6}_\d{8}_\d{6}.\d{3}.dat$' % args.run_number, f)]
    for matched_file in files:
        PlotUpdater.check_and_plot(os.path.join(args.data_folder, matched_file), run_plot_folder)

    # setup filesystem monitor to generate new plots on the fly
    event_handler = TransientDataHandler(args.run_number, run_plot_folder)
    observer = Observer()
    observer.schedule(event_handler, args.data_folder, recursive=False)
    observer.start()

    # keep observer running until Ctrl-C is pressed
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

