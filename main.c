#include <libbladeRF.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <volk/volk.h>
#include <volk/volk_complex.h>
#include <unistd.h>
#include <pthread.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <sys/types.h>
#include <errno.h>
#include <sys/time.h>
#define SA struct sockaddr
// Trigger threshold in radians:
//   0.1  = 50 ps at 320 MHz
//   0.05 = 25 ps at 320 MHz
//   0.02 = 10 ps at 320 MHz
#define TRIG_THRESH 0.05

#define NUM_THREADS 4
#define BUF_SIZE    32768

#define PORT        8042
#define TCP_BUF_LEN 1024

// globals for trigger level, written only at init
float *mean, *stddev;

// scheduling / buffer processing, protected by buffer_mutex
pthread_mutex_t buffer_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t filled_cv = PTHREAD_COND_INITIALIZER;
pthread_cond_t processed_cv = PTHREAD_COND_INITIALIZER;
int filled_buffers = 0;
int taken_buffers = 0;
int16_t *buffers[NUM_THREADS];
int buf_num[NUM_THREADS];

// trigger detection logic, protected by trigger_mutex
pthread_mutex_t trigger_mutex = PTHREAD_MUTEX_INITIALIZER;
bool triggered = false;
bool tcp_trigger_enable = false;
bool trigger_enabled = false;
int trig_id = 0;
int run_id = 0;

bool running = true;

void *tcp_handler(void *args) {
    char buff[TCP_BUF_LEN]; 
    int sockfd, connfd, len;

    struct sockaddr_in servaddr, cli;

    int last_event_id;

    sockfd = socket(AF_INET, SOCK_STREAM, 0); 
    if (sockfd == -1) { 
        printf("socket creation failed...\n"); 
        exit(0); 
    } 

    int yes = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));

    bzero(&servaddr, sizeof(servaddr)); 

    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY); 
    servaddr.sin_port = htons(PORT); 

    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        printf("socket bind failed...\n");
        exit(0);
    }

    if ((listen(sockfd, 5)) != 0) { 
        printf("Listen failed...\n"); 
        exit(0); 
    } 

    while(running) {
        printf("Waiting for connection...\n");
        connfd = accept(sockfd, (SA*)&cli, &len);
        printf("Connection accepted.\n");
        if (connfd < 0) {
            printf("server acccept failed...\n");
            exit(0);
        } else {
            bool conn_open = true;
            int read_count = 0;
            int read_err = 0;
            while(conn_open) {
                bzero(buff, TCP_BUF_LEN);
                read_count = read(connfd, buff, sizeof(buff));
                read_err = errno;
                if (read_count <= 0) {
                    printf("Error while reading from Socket: %d\n", read_count);
                    printf("Errno: %s\n", strerror(read_err));
                    conn_open = false;
                } else {
                    if (strncmp(buff, "sdr_init", 8) == 0) {
                        write(connfd, "ack\n", sizeof("ack\n")-1);
                    }
                    if (strncmp(buff, "sdr_start", 9) == 0) {
                        if (run_id == 0) {
                            FILE* file = fopen("data/run_num.dat", "rb");
                            if (file != NULL) {
                                pthread_mutex_lock(&trigger_mutex);
                                fread(&run_id, sizeof(int), 1, file);
                                pthread_mutex_unlock(&trigger_mutex);
                                fclose(file);
                            }
                        }

                        pthread_mutex_lock(&trigger_mutex);
                        tcp_trigger_enable = true;
                        trig_id = 0;
                        last_event_id = 0;
                        run_id++;
                        FILE* file = fopen("data/run_num.dat", "wb");
                        if (file != NULL) {
                            fwrite(&run_id, sizeof(int), 1, file);
                            fclose(file);
                        }
                        pthread_mutex_unlock(&trigger_mutex);

                        write(connfd, "ack\n", sizeof("ack\n")-1);
                        printf("Starting run %d.\n", run_id);
                    }
                    if (strncmp(buff, "sdr_report_events", 17) == 0) {
                        pthread_mutex_lock(&trigger_mutex);
                        bool event_happened = triggered;
                        int event_id = trig_id;
                        triggered = false;
                        pthread_mutex_unlock(&trigger_mutex);

                        char trigger_num[64]; // 6 digit ID + space + 6 digit difference + \n + \0
                        sprintf(trigger_num, "%d %d\n", event_id, event_id - last_event_id);
                        write(connfd, trigger_num, strlen(trigger_num));
                        last_event_id = event_id;
                    }
                    if (strncmp(buff, "sdr_stop", 8) == 0) {
                        pthread_mutex_lock(&trigger_mutex);
                        tcp_trigger_enable = false;
                        pthread_mutex_unlock(&trigger_mutex);
                        write(connfd, "ack\n", sizeof("ack\n")-1);
                    }
                }
            }
            printf("Connection closed.\n");
        }
    }
}

void *process_buffer(void *args) {
    unsigned int alignment = volk_get_alignment();

    lv_32fc_t *rx_samples_fc, *rx_samples_dc_fc, *rx_samples_dc_ma_fc, *dc_lo_fc;
    float *phase, *negphase, *zeros, *buf_mean, *buf_stddev;
    uint32_t *max, *min;
    bool trigger = false;
    bool apply_mean = false;
    int read_buf, i, buffer_mask;

    // Allocate all buffers
    rx_samples_fc = (lv_32fc_t*)volk_malloc(sizeof(lv_32fc_t)*BUF_SIZE, alignment);
    rx_samples_dc_fc = (lv_32fc_t*)volk_malloc(sizeof(lv_32fc_t)*BUF_SIZE, alignment);
    rx_samples_dc_ma_fc = (lv_32fc_t*)volk_malloc(sizeof(lv_32fc_t)*(BUF_SIZE), alignment);
    dc_lo_fc = (lv_32fc_t*)volk_malloc(sizeof(lv_32fc_t)*BUF_SIZE, alignment);
    phase = (float*)volk_malloc(sizeof(float)*(BUF_SIZE), alignment);
    negphase = (float*)volk_malloc(sizeof(float)*(BUF_SIZE), alignment);
    zeros = (float*)volk_malloc(sizeof(float)*(BUF_SIZE), alignment);
    buf_mean = (float*)volk_malloc(sizeof(float), alignment);
    buf_stddev = (float*)volk_malloc(sizeof(float), alignment);

    max = (uint32_t*)volk_malloc(sizeof(uint32_t), alignment);
    min = (uint32_t*)volk_malloc(sizeof(uint32_t), alignment);

    // Prepare Zero-Buffer (for inversion)
    for (i = 0; i < BUF_SIZE; i++) {
        zeros[i] = 0.0f;
    }

    // Prepare downconversion LO buffer
    for (i = 0; i < BUF_SIZE; i = i + 4) {
        dc_lo_fc[i+0] = lv_cmake(1.0f, 0.0f);
        dc_lo_fc[i+1] = lv_cmake(0.0f, -1.0f); // low-side LO
        //dc_lo_fc[i+1] = lv_cmake(0.0f, 1.0f); // high-side LO
        dc_lo_fc[i+2] = lv_cmake(-1.0f, 0.0f);
        dc_lo_fc[i+3] = lv_cmake(0.0f, 1.0f); // low-side LO
        //dc_lo_fc[i+3] = lv_cmake(0.0f, -1.0f); // high-side LO
    }

    printf("Thread startup complete.\n");

    // Main processing loop
    while (running) {
        // wait for a buffer to become filled
        pthread_mutex_lock(&buffer_mutex);
        while ((filled_buffers ^ taken_buffers) == 0) {
            pthread_cond_wait(&filled_cv, &buffer_mutex);
        }

        read_buf = -1;
        buffer_mask = filled_buffers ^ taken_buffers;
        for (i = 0; i < NUM_THREADS; i++) {
            if (buffer_mask & (1 << i)) {
                taken_buffers |= (1 << i);
                read_buf = i;
                break;
            }
        }
        if (read_buf == -1) {
            printf("Did not pick a buffer!\n");
            while(1) {};
        }
        *buf_mean = *mean;
        pthread_mutex_unlock(&buffer_mutex);

        // calculate new trigger level every 16 acquisitions
        if ((buf_num[read_buf] & 0x0f) == 0) {
            apply_mean = true;
        }

        // process the selected buffer
        volk_16ic_convert_32fc(rx_samples_fc, (const lv_16sc_t*)buffers[read_buf], BUF_SIZE);
        volk_32fc_x2_multiply_32fc(rx_samples_dc_fc, rx_samples_fc, dc_lo_fc, BUF_SIZE);
        volk_32f_x2_add_32f((float *)rx_samples_dc_ma_fc, (float *)rx_samples_dc_fc, (float *)rx_samples_dc_fc+2, 2*(BUF_SIZE-4));
        volk_32f_x2_add_32f((float *)rx_samples_dc_ma_fc, (float *)rx_samples_dc_ma_fc, (float *)rx_samples_dc_fc+4, 2*(BUF_SIZE-4));
        volk_32f_x2_add_32f((float *)rx_samples_dc_ma_fc, (float *)rx_samples_dc_ma_fc, (float *)rx_samples_dc_fc+6, 2*(BUF_SIZE-4));
        volk_32fc_s32f_atan2_32f(phase, rx_samples_dc_ma_fc, 1.0f, BUF_SIZE-4);
        volk_32f_x2_subtract_32f(negphase, zeros, phase, BUF_SIZE-4);
        volk_32f_index_max_32u(max, phase, BUF_SIZE-4);
        volk_32f_index_max_32u(min, negphase, BUF_SIZE-4);
        if ((phase[*max] - *buf_mean) > TRIG_THRESH) {
            trigger = true;
        }
        if ((*buf_mean - phase[*min]) > TRIG_THRESH) {
            trigger = true;
        }
        if (trigger) {
            pthread_mutex_lock(&trigger_mutex);
            bool trig_enabled = trigger_enabled;
            int run_id_copy;
            int trig_id_copy;
            if (trig_enabled) {
                triggered = true;   // signal trigger to TCP handler thread
                trig_id++;          // increment global trigger ID counter 
                trig_id_copy = trig_id; // copy trigger ID to local variable
                run_id_copy = run_id;
            }
            pthread_mutex_unlock(&trigger_mutex);

            if (trig_enabled) {
                int millisec;
                struct tm* tm_info;
                struct timeval tv;
                char trigger_fname[64];
                char time_string[32];

                gettimeofday(&tv, NULL);
                millisec = lrint(tv.tv_usec/1000.0); // Round to nearest millisec
                if (millisec>=1000) { // Allow for rounding up to nearest second
                    millisec -=1000;
                    tv.tv_sec++;
                }

                tm_info = localtime(&tv.tv_sec);
                strftime(time_string, 32, "%Y%m%d_%H%M%S", tm_info);
                sprintf(trigger_fname, "data/run%06d_trigger%06d_%s.%03d.dat", run_id_copy, trig_id_copy, time_string, millisec);
                printf("Triggered. Storing to %s.\n", trigger_fname);

                FILE* file = fopen(trigger_fname, "wb");
                fwrite(phase, sizeof(float), BUF_SIZE-4, file);
                fclose(file);
            }
            trigger = false;
        }

        if (apply_mean) {
            volk_32f_stddev_and_mean_32f_x2(buf_stddev, buf_mean, phase, BUF_SIZE-4);
        }

        // apply new trigger levels, globally enable/disable trigger, mark buffer free
        pthread_mutex_lock(&buffer_mutex);
        if (apply_mean) {
            *mean = *buf_mean;
            apply_mean = false;
            // globally enable/disable the trigger when asked for via TCP
            pthread_mutex_lock(&trigger_mutex);
            if (tcp_trigger_enable && !trigger_enabled) {
                printf("Enabling trigger.\n");
                fflush(stdout);
                trigger_enabled = true;
            }
            if (!tcp_trigger_enable && trigger_enabled) {
                printf("Disabling trigger.\n");
                fflush(stdout);
                trigger_enabled = false;
            }
            pthread_mutex_unlock(&trigger_mutex);
        }
        taken_buffers  &= ~(1 << read_buf);
        filled_buffers &= ~(1 << read_buf);
        pthread_cond_signal(&processed_cv);
        pthread_mutex_unlock(&buffer_mutex);
    }
}

int main(int argc, char *argv[])
{
    int status, i, write_buf;
    int bladerf_version = 1;
    unsigned int alignment = volk_get_alignment();
    pthread_t threads[NUM_THREADS];
    pthread_t tcp_thread;

    struct bladerf *dev = NULL;
    struct bladerf_devinfo dev_info;

    // initialize bladeRF hardware
    bladerf_init_devinfo(&dev_info);
    status = bladerf_open_with_devinfo(&dev, &dev_info);
    if (status != 0) {
        fprintf(stderr, "Unable to open device: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    if (strcmp(bladerf_get_board_name(dev), "bladerf2") == 0) {
        fprintf(stdout, "bladeRF 2.0 detected\n");
        bladerf_version = 2;
    }

    if (bladerf_version == 1) {
        status = bladerf_set_smb_mode(dev, BLADERF_SMB_MODE_INPUT);
        if (status != 0) {
            fprintf(stderr, "Failed to set SMB mode: %s\n",
                    bladerf_strerror(status));
            return -1;
        }
    }
    if (bladerf_version == 2) {
        status = bladerf_set_pll_refclk(dev, 40000000);
        if (status != 0) {
            fprintf(stderr, "Failed to set PLL reference frequency: %s\n",
                    bladerf_strerror(status));
            return -1;
        }
        status = bladerf_set_pll_enable(dev, true);
        if (status != 0) {
            fprintf(stderr, "Failed to enable reference PLL: %s\n",
                    bladerf_strerror(status));
            return -1;
        }
    }

    status = bladerf_set_frequency(dev, BLADERF_CHANNEL_RX(0), 314400000);
    if (status != 0) {
        fprintf(stderr, "Failed to set frequency: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    status = bladerf_set_sample_rate(dev, BLADERF_CHANNEL_RX(0), 22400000, NULL);
    if (status != 0) {
        fprintf(stderr, "Failed to set samplerate: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    status = bladerf_set_bandwidth(dev, BLADERF_CHANNEL_RX(0), 10000000, NULL);
    if (status != 0) {
        fprintf(stderr, "Failed to set bandwidth: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    if (bladerf_version == 2) {
        status = bladerf_set_gain_mode(dev, BLADERF_CHANNEL_RX(0), BLADERF_GAIN_AUTOMATIC);
        if (status != 0) {
            fprintf(stderr, "Failed to set gain: %s\n", bladerf_strerror(status));
            return -1;
        }
    } else {
        status = bladerf_set_gain(dev, BLADERF_CHANNEL_RX(0), 35);
        if (status != 0) {
            fprintf(stderr, "Failed to set gain: %s\n", bladerf_strerror(status));
            return -1;
        }
    }

    status = bladerf_sync_config(dev, BLADERF_RX_X1, BLADERF_FORMAT_SC16_Q11,
            16, BUF_SIZE, 8, 3500);
    if (status != 0) {
        fprintf(stderr, "Failed to configure RX sync interface: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    if (bladerf_version == 1) {
        // disable dithering for RX phase coherency
        // bladeRF v2 PLLs don't have dithering enabled by default
        status = bladerf_lms_write(dev, 0x24, 0x08);
        if (status != 0) {
            fprintf(stderr, "Failed to disable PLL dithering: %s\n",
                    bladerf_strerror(status));
            return -1;
        }
    }

    // allocate RX sample buffers
    for (i = 0; i < NUM_THREADS; i++) {
        buffers[i] = (int16_t*)volk_malloc(sizeof(int16_t)*2*BUF_SIZE, alignment);
        if (buffers[i] == NULL) {
            perror("Failed to allocate RX sample buffers");
            status = BLADERF_ERR_MEM;
            return -1;
        }
    }

    // create TCP thread
    pthread_create(&tcp_thread, NULL, tcp_handler, NULL);

    // create DSP threads
    for (i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, process_buffer, NULL);
    }

    status = bladerf_enable_module(dev, BLADERF_RX, true);
    if (status != 0) {
        fprintf(stderr, "Failed to enable RX: %s\n", bladerf_strerror(status));
        return -1;
    }

    mean = (float*)volk_malloc(sizeof(float), alignment);
    stddev = (float*)volk_malloc(sizeof(float), alignment);
    running = true;
    int rx_count = 0;

    while(1) {
        pthread_mutex_lock(&buffer_mutex);
        while (filled_buffers == (1 << NUM_THREADS) - 1) {
            pthread_cond_wait(&processed_cv, &buffer_mutex);
        }
        write_buf = -1;
        for (i = 0; i < NUM_THREADS; i++) {
            if ((filled_buffers & (1 << i)) == 0) {
                write_buf = i;
                break;
            }
        }
        if (write_buf == -1) {
            printf("No buffer selected for writing!\n");
            return 0;
        }
        pthread_mutex_unlock(&buffer_mutex);

        // fill buffer
        buf_num[write_buf] = rx_count;
        rx_count++;
        bladerf_sync_rx(dev, buffers[write_buf], BUF_SIZE, NULL, 5000);

        // mark buffer as full
        pthread_mutex_lock(&buffer_mutex);
        filled_buffers |= (1 << write_buf);
        pthread_cond_broadcast(&filled_cv);
        pthread_mutex_unlock(&buffer_mutex);
    }

    bladerf_close(dev);
    return status;
}
