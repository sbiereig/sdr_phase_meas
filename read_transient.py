#!/usr/bin/python3
import argparse
import numpy as np
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Plot phase transients from file.")
parser.add_argument("filename", type=str, help="Name of the file to read from")
parser.add_argument("--hist", action="store_true", help="Plot histogram")
args = parser.parse_args()

fs = 22.4e6
f_in = 320e6

data = np.fromfile(args.filename, dtype=np.float32)
data = np.unwrap(data)
data = data / (2 * np.pi * f_in) * 1e9
data = data - np.mean(data)
t = 1.0 / fs * np.arange(len(data)) * 1e6

np.save("trigger.npy", np.stack((t, data)))

if not args.hist:
    plt.plot(t, data * 1000)
    plt.xlabel("Time [us]")
    plt.ylabel("Phase [ps]")
    plt.grid()
    plt.show()

if args.hist:
    data = (data - np.mean(data)) * 1000
    std = np.std(data)
    print(f"Standard deviation: {std:.02f} ps")
    plt.hist(data, 64)
    plt.xlabel("Phase [ps]")
    plt.ylabel("Number of Events")
    plt.grid()
    plt.show()

