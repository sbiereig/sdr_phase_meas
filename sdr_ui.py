#!/usr/bin/env python3
""" SEU monitoring for GSI testbeam."""

import logging
import sys
import termios
import atexit
import time
import socket
from select import select


class Keyboard:
    def __init__(self):
        '''Creates a KBHit object that you can call to do various keyboard things.
        '''
        # Save the terminal settings
        self.fd = sys.stdin.fileno()
        self.new_term = termios.tcgetattr(self.fd)
        self.old_term = termios.tcgetattr(self.fd)
        # New terminal setting unbuffered
        self.new_term[3] = (self.new_term[3] & ~termios.ICANON & ~termios.ECHO)
        termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.new_term)

        # Support normal-terminal reset at exit
        atexit.register(self.set_normal_term)

    def set_normal_term(self):
        ''' Resets to normal terminal.  On Windows this is a no-op.
        '''
        termios.tcsetattr(self.fd, termios.TCSAFLUSH, self.old_term)

    def getch(self):
        ''' Returns a keyboard character after kbhit() has been called.
            Should not be called in the same program as getarrow().
        '''
        s = ''
        return sys.stdin.read(1)

    def kbhit(self):
        ''' Returns True if keyboard character was hit, False otherwise.
        '''
        dr, dw, de = select([sys.stdin], [], [], 0)
        return dr != []


def update_bank(tester, val):
    """Writes an encoded bank to the capacitor bank override register."""
    conf0 = tester.lpgbt.read_reg(Lpgbt.CLKGLFCONFIG0)
    if val & 0x100:
        conf0 |= 0x10
    else:
        conf0 &= 0xef
    tester.lpgbt.write_reg(Lpgbt.CLKGLFCONFIG0, conf0)
    tester.lpgbt.write_reg(Lpgbt.CLKGOVERRIDECAPBANK, val & 0xff)

def sdr_ui():
    logging.basicConfig(level=logging.INFO)
    logging.info("SDR UI Startup.")
    logging.info("Keybindings:")
    logging.info("  a - Arm (Start new run)")
    logging.info("  d - Disarm (Stop run)")

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(('127.0.0.1', 8042))
    s.send(b"sdr_stop")

    kb = Keyboard()
    try:
        while True:
            time.sleep(0.1)
            if kb.kbhit():
                c = kb.getch()
                if c in (chr(27), 'q'):
                    logging.info("Ending on user request (ESC or q)")
                    break
                if c == 'a':
                    s.send(b"sdr_stop\n")
                    s.send(b"sdr_start\n")
                    logging.info("Starting new run.")
                if c == 'd':
                    s.send(b"sdr_stop\n")
                    logging.info("Stopping run.")
    except KeyboardInterrupt:
        logging.info("Ending on user request (CTRL+C)")

    s.send(b"sdr_stop")
    s.close()
    kb.set_normal_term()
    return True


if __name__ == "__main__":
    sdr_ui()
